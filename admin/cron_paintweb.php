<?php

/**
 * Perform the cleanup task for removing any obsolete images saved by PaintWeb.
 *
 * @uses $CFG
 */
function paintweb_cron_cleanup () {
    global $CFG;

    if (empty($CFG->paintwebImagesFolder)) {
        $CFG->paintwebImagesFolder = 'paintweb_images';
    }

    $pwtable = 'paintweb_images';
    $pwfolder = $CFG->dataroot . '/' . $CFG->paintwebImagesFolder;

    // Get the list of tables being tracked, known to have records with images 
    // saved by PaintWeb.
    $tables = get_fieldset_select($pwtable, 'DISTINCT(tbl)');
    if (!$tables) {
        $tables = array();
    }

    // Delete obsolete images in the PaintWeb table.
    foreach ($tables as $tbl) {
        delete_records_select($pwtable, "NOT EXISTS (SELECT b.id FROM 
    {$CFG->prefix}$tbl AS b WHERE {$CFG->prefix}$pwtable.tbl='$tbl' AND 
    {$CFG->prefix}$pwtable.rid=b.id)");
    }

    // Get the list of image file names being tracked in the PaintWeb table.
    $tblimages = get_fieldset_select($pwtable, 'DISTINCT(img)');
    if (!$tblimages) {
        $tblimages = array();
    }

    $cwd = getcwd();
    chdir($pwfolder);

    // Get the list of image file names inside the physical folder of PaintWeb.
    $images = glob("*.{png,jpg}", GLOB_BRACE);
    if (!$images) {
        $images = array();
    }

    // Remove any file which is not in the database.
    foreach ($images as $img) {
        if (!in_array($img, $tblimages)) {
            unlink($img);
        }
    }

    chdir($cwd);
}


// vim:set spell spl=en fo=tanqrowcb tw=80 ts=4 sw=4 sts=4 sta et noai nocin fenc=utf-8 ff=unix: 

